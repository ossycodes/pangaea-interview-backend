<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Topic;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase, HasFactory;

    /**
     * @test
     */
    public function url_is_required_to_subscribe_to_a_topic()
    {
        $topic = Topic::factory()->create();

        $this->json('POST', route('api.subscription.create', [
            'topic' => $topic->topic
        ]))
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'url' => []
                ]
            ]);
    }

    /**
     * @test
     */
    public function url_must_be_of_valid_format_to_subscribe_to_a_topic()
    {
        $topic = Topic::factory()->create();

        $this->json('POST', route('api.subscription.create', [
            'topic' => $topic->topic
        ]), [
            'url' => 'invalidurlformat'
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'url' => []
                ]
            ]);
    }

    /**
     * @test
     */
    public function a_topic_can_be_subscribed_to()
    {
        $topic = Topic::factory()->create();
        $url = 'https://google.com';

        $this->json('POST', route('api.subscription.create', [
            'topic' => $topic->topic
        ]), [
            'url' => $url
        ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'status' => Response::HTTP_OK
            ]);

        $this->assertDatabaseHas('topics', [
            'topic' => $topic->topic
        ]);

        $this->assertDatabaseHas('subscriptions', [
            'url' => $url
        ]);
    }
}
