<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Topic;
use App\Models\Subscription;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PublishEventTest extends TestCase
{
    use RefreshDatabase, HasFactory;

    /**
     * @test
     */
    public function message_is_required_to_publish_event()
    {
        $topic = Topic::factory()->create();

        $this->json('POST', route('api.event.publish', [
            'topic' => $topic->topic
        ]))
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'message' => []
                ]
            ]);
    }

    /**
     * @test
     */
    public function event_can_be_published_to_all_subscribed_urls()
    {
        Http::fake();

        //create topic
        $topic = Topic::factory()->create([
            'topic' => 'topic1'
        ]);
        $url = 'https://www.google.com/';

        //create subscription for $topic
        $subscription = Subscription::factory()->create([
            'topic_id' => $topic->id,
            'url' => $url
        ]);

        $this->json(
            'POST',
            route('api.event.publish', [
                'topic' => $topic
            ]),
            [
                'message' => 'hello world'
            ]
        )
            ->assertStatus(Response::HTTP_OK);
   
        Http::assertSent(function ($request) use ($url) {
            return $request->url() == $url
                &&
                $request['topic'] == 'topic1' &&
                $request['message'] == 'hello world';
        });
    }
}
