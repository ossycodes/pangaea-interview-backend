<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublishEventController;
use App\Http\Controllers\CreateSubscriptionController;
use App\Http\Controllers\EventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/subscribe/{topic}', [CreateSubscriptionController::class, 'store'])->name('api.subscription.create');
Route::post('/publish/{topic:topic}', [PublishEventController::class, 'update'])->name('api.event.publish');
Route::post('/event', [EventController::class, 'index'])->name('api.events.all');
