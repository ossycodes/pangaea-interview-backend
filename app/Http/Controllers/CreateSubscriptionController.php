<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSubscription;
use Symfony\Component\HttpFoundation\Response;

class CreateSubscriptionController extends Controller
{
    /**
     * Create Subscription for topic
     * 
     * @param Request $request
     * @param string $topic
     */
    public function store(StoreSubscription $request, $topic)
    {
        $data = $request->validated();

        /**
         * I can move this queries to a repository (repository pattern)
         * but since it's a small application, having the query directly
         * in the controller should be okay.
         */

        //create topic if not exists
        $topic = Topic::firstOrCreate([
            'topic' => $topic
        ]);

        //create subscription for that topic
        $topic->subscriptions()->create($data);

        return response()->json([
            'status' => Response::HTTP_OK,
            'message' => 'subscription created',
        ]);
    }
}
