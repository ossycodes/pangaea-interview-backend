<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Http\Requests\PublishEvent;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;

class PublishEventController extends Controller
{
    public function update(Topic $topic, PublishEvent $request)
    {
        $data = $request->validated();

        // explicitly added a select('url') for better performance rather than selecting all columns *
        $subscriptions = $topic->subscriptions()->select('url')->get();

        foreach ($subscriptions as $subscription) {

            $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ])->post(
                $subscription->url,
                [
                    'topic' => $topic->topic,
                    'message' => $data['message']
                ]
            );

            if ($response->failed()) {
                $response->throw();
            }
        }

        return response()->json([
            'status' => Response::HTTP_OK,
            'message' => 'event published',
        ]);
    }
}
