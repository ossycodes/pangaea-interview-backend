<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request)
    {
        //log the message and topic published
        logger(request()->all());

        return response()->json([
            'status' => 'ok'
        ]);
    }
}
