## Pangaea BE Coding Challenge

Link to Coding Challenge:

- [Coding Challenge](https://pangaea-interviews.now.sh/be).

Framework Used:

- Laravel

## Approach

### First modeled the relationship between a Topic and a Subscription, which is a One to Many Relationship, then went ahead to generate the neccessary migrations needed to create the database, a topics table to store all topics available and a subscriptions table to stores all subscriptions for a topic.

### when the subscriptions endpoint is triggered `/subscribe/{TOPIC}` I create a new row in the topics table if the topic does not already exist, and then proceed to create a subscription for the just created topic.

### when the publish events endpoint is triggered `/publish/{TOPIC}` the code just checks the subscriptions table for all urls that have been subscribed to  the said topic, for each URL returned from the database, a POST request to them is made forwarding the JSON body `message` and the `topic` just recieved as seen below:

```
{
    "topic": "topic",
    "message": "message"
}
```
**Laravel framework adds the prefix `/api` to API endpoints**

- Create Subscrption `POST localhost:8000/api/subscribe/topic1`
- BODY : { "url": "http://localhost:8000/event"}

- Publish Event `POST localhost:8000/api/publish/topic1`
- BODY { "message": "hello"}

## Tests

```
$ php artisan test //runs all tests
$ php artisan test --filter SubscriptionTest //runs only the SubscriptionTest
$ php artisan test --filter PublishEventTest //runs only the PublishEventTest
```